import java.io.Serializable;
import java.net.InetAddress;

import java.util.HashMap;
import java.util.ArrayList;

public class MPacket implements Serializable {

    /*The following are the type of events*/
    public static final int SEQ_REQ = 96;
    public static final int SEQ_RESP = 97;
    public static final int NS_HELLO = 98;
    public static final int NS_RESP = 99;
    public static final int HELLO = 100;
    public static final int ACTION = 200;
    public static final int ACK = 300;
    public static final int TOKEN = 400;
    public static final int ACK_TOKEN = 401;

    /*The following are the specific action 
    for each type*/
    /*Initial Hello*/
    public static final int HELLO_INIT = 101;
    /*Response to Hello*/
    public static final int HELLO_RESP = 102;

    /*Action*/
    public static final int UP = 201;
    public static final int DOWN = 202;
    public static final int LEFT = 203;
    public static final int RIGHT = 204;
    public static final int FIRE = 205;
    public static final int PROJECTILE = 206;
    public static final int ACK_ACTION = 207;
    public static final int SEND_TOKEN = 208;
    
    
    
    //These fields characterize the event  
    public int type;
    public int event; 

    //The name determines the client that initiated the event
    public String name;
    public InetAddress ipAddress;
    public int port;
    public int listener_port;

    //List of clients
    public HashMap <String, ClientInfo> clientHash;
    public HashMap <String,Integer> priority;
   
    //Server's ip address & port
    public String sAddr = "ug145";
    public int sPort = 8001;

    //The sequence number of the event
    public int sequenceNumber;
    public int tokenSequenceNumber;

    //These are used to initialize the board
    public int mazeSeed;
    public int mazeHeight;
    public int mazeWidth; 
    public Player[] players;

    public MPacket(int type, int event){
        this.type = type;
        this.event = event;
    }
    
    public MPacket(String name, int type, int event){
        this.name = name;
        this.type = type;
        this.event = event;
        this.sequenceNumber = 0;
        this.tokenSequenceNumber = 0;
    }
    
    public MPacket(String name, int type, int event, InetAddress ipAddress){
        this.name = name;
        this.type = type;
        this.event = event;
        this.ipAddress = ipAddress;
    }
    
    public MPacket(int type, InetAddress ipAddress){
        this.type = type;
        this.ipAddress = ipAddress;
    }
    
    public String toString(){
        String typeStr;
        String eventStr;
        
        switch(type){
        	case 96:
        		typeStr = "SEQ_REQ";
        		break;
        	case 97:
        		typeStr = "SEQ_RESP";
        		break;
        	case 98:
        		typeStr = "NS_HELLO";
        		break;
	        case 99:
	            typeStr = "NS_RESP";
	            break;
           case 100:
                typeStr = "HELLO";
                break;
            case 200:
                typeStr = "ACTION";
                break;
            case 300:
                typeStr = "ACK";
                break;
            case 400:
                typeStr = "TOKEN";
                break;
            case 401:
                typeStr = "ACK_TOKEN";
                break;
            default:
                typeStr = Integer.toString(type);
                break;        
        }
        
        switch(event){
            case 101:
                eventStr = "HELLO_INIT";
                break;
            case 102:
                eventStr = "HELLO_RESP";
                break;
            case 201:
                eventStr = "UP";
                break;
            case 202:
                eventStr = "DOWN";
                break;
            case 203:
                eventStr = "LEFT";
                break;
            case 204:
                eventStr = "RIGHT";
                break;
            case 205:
                eventStr = "FIRE";
                break;
            case 206:
                eventStr = "PROJECTILE";
                break;
            case 207:
                eventStr = "ACK_ACTION";
                break;
            case 208:
                eventStr = "SEND_TOKEN";
                break;
            default:
                eventStr = Integer.toString(event);
                break;        
        }
        //MPACKET(NAME: name, <typestr: eventStr>, SEQNUM: sequenceNumber)
        String retString = String.format("MPACKET(NAME: %s, <%s: %s>, SEQNUM: %s)", name, 
            typeStr, eventStr, sequenceNumber);
        return retString;
    }

}
