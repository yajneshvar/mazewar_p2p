/*
Copyright (C) 2004 Geoffrey Alan Washburn
   
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
   
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
   
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
USA.
*/
  
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BorderFactory;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Hashtable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.Iterator;

/**
 * The entry point and glue code for the game.  It also contains some helpful
 * global utility methods.
 * @author Geoffrey Washburn &lt;<a href="mailto:geoffw@cis.upenn.edu">geoffw@cis.upenn.edu</a>&gt;
 * @version $Id: Mazewar.java 371 2004-02-10 21:55:32Z geoffw $
 */

public class Mazewar extends JFrame {

	
		private String name = null;
	
		
        /**
         * The default width of the {@link Maze}.
         */
        private final int mazeWidth = 20;

        /**
         * The default height of the {@link Maze}.
         */
        private final int mazeHeight = 10;

        /**
         * The default random seed for the {@link Maze}.
         * All implementations of the same protocol must use 
         * the same seed value, or your mazes will be different.
         */
        private final int mazeSeed = 42;

        /**
         * The {@link Maze} that the game uses.
         */
        private Maze maze = null;

        /**
         * The Mazewar instance itself. 
         */
        private Mazewar mazewar  = null;
        private MSocket mSocket  = null;
        private MSocket nSocket  = null;
        private MServerSocket myListenerSocket = null;
        private HashMap <String, ClientInfo> clientHash = null;
        private HashMap <String, MSocket> socketHash = null;
        private String nextClient = null;
        private ObjectOutputStream out = null;
        private ObjectInputStream in = null;
        private PacketComparator comparator = null;
        private int myId = -1;

        /**
         * The {@link GUIClient} for the game.
         */
        private GUIClient guiClient = null;
        
        
        /**
         * A map of {@link Client} clients to client name.
         */
        private Hashtable<String, Client> clientTable = null;

        /**
         * A queue of Acks.
         */
        private BlockingQueue ackQueue = null;
        
        /**
         * A queue of events.
         */
        private BlockingQueue eventQueue = null;
        
        /**
         * A queue of events.
         */
        private BlockingQueue tokenAckQueue = null;
        
        /**
         * A queue of sequence numbers for packets from the sever.
         */
        private BlockingQueue seqQueue = null;
        
        /**
         * A queue of packets recieved from other clients.
         */
        private PriorityBlockingQueue<MPacket> clientQueue = null;
        
        /**
         * A queue to receive tokens.
         */
        private BlockingQueue tokenQueue = null;
        
        /**
         * A queue of packets recieved from Naming Service.
         */
        private BlockingQueue NSQueue = null;
        
        /**
         * The panel that displays the {@link Maze}.
         */
        private OverheadMazePanel overheadPanel = null;

        /**
         * The table the displays the scores.
         */
        private JTable scoreTable = null;
        
        /** 
         * Create the textpane statically so that we can 
         * write to it globally using
         * the static consolePrint methods  
         */
        private static final JTextPane console = new JTextPane();
      
        /** 
         * Write a message to the console followed by a newline.
         * @param msg The {@link String} to print.
         */ 
        public static synchronized void consolePrintLn(String msg) {
                console.setText(console.getText()+msg+"\n");
        }
        
        /** 
         * Write a message to the console.
         * @param msg The {@link String} to print.
         */ 
        public static synchronized void consolePrint(String msg) {
                console.setText(console.getText()+msg);
        }
        
        /** 
         * Clear the console. 
         */
        public static synchronized void clearConsole() {
           console.setText("");
        }
        
        /**
         * Static method for performing cleanup before exiting the game.
         */
        public static void quit() {
                // Put any network clean-up code you might have here.
                // (inform other implementations on the network that you have 
                //  left, etc.)
                

                System.exit(0);
        }
       
        /** 
         * The place where all the pieces are put together. 
         */
        public Mazewar(String serverHost, int serverPort) throws IOException,
                                                ClassNotFoundException {
                super("ECE419 Mazewar");
                consolePrintLn("ECE419 Mazewar started!");
                
                // Create the maze
                maze = new MazeImpl(new Point(mazeWidth, mazeHeight), mazeSeed);
                assert(maze != null);
                
                // Have the ScoreTableModel listen to the maze to find
                // out how to adjust scores.
                ScoreTableModel scoreModel = new ScoreTableModel();
                assert(scoreModel != null);
                maze.addMazeListener(scoreModel);
                
                // Throw up a dialog to get the GUIClient name.
                name = JOptionPane.showInputDialog("Enter your name");
                if((name == null) || (name.length() == 0)) {
                  Mazewar.quit();
                }
                
                nSocket = new MSocket(serverHost, serverPort);
                myListenerSocket = new MServerSocket(0); 
                
                //send hello to naming service
                MPacket ns_hello = new MPacket(name, MPacket.NS_HELLO, MPacket.HELLO_INIT);
                //tell the naming server what my port number is 
                ns_hello.listener_port = myListenerSocket.getLocalPort();
                ns_hello.port = nSocket.getLocalPort();
                ns_hello.ipAddress = nSocket.getLocalAddress();
                ns_hello.clientHash = clientHash;
                ns_hello.mazeWidth = mazeWidth;
                ns_hello.mazeHeight = mazeHeight;
                if(Debug.debug) System.out.println("Sending hello to NS");
                nSocket.writeObjectNoError(ns_hello);
                if(Debug.debug) System.out.println("NS_hello sent");
                //Receive response from naming service server
                MPacket ns_resp = (MPacket)nSocket.readObjectNoError();
                if(Debug.debug) System.out.println("Received response from server");
                
                //now get the next client that you need to send the token ring too
                myId = ns_resp.priority.get(name);
                nextClient = getNextClient(ns_resp.priority);
                
                clientHash = ns_resp.clientHash;
                socketHash = new HashMap();
     
                comparator = new PacketComparator();
                clientQueue = new PriorityBlockingQueue<MPacket>(11,comparator);
                //Got the server location for naming socket. Create a new connection
               // mSocket = new MSocket(ns_resp.sAddr, ns_resp.sPort);

                /*
                //Send hello packet to server
                MPacket hello = new MPacket(name, MPacket.HELLO, MPacket.HELLO_INIT);
                hello.ipAddress = InetAddress.getLocalHost();
                hello.port = mSocket.getLocalPort(); // get the local port that is bound to this socket
                hello.clientHash = clientHash;
                hello.mazeWidth = mazeWidth;
                hello.mazeHeight = mazeHeight;
                
                //FIXME!!!!
                if(Debug.debug) System.out.println("Sending hello");
                mSocket.writeObjectNoError(hello);
                if(Debug.debug) System.out.println("hello sent");
                //Receive response from server
                MPacket resp = (MPacket)mSocket.readObjectNoError();
                if(Debug.debug) System.out.println("Received response from server");
				*/
                //Initialize queues
                eventQueue = new LinkedBlockingQueue<MPacket>();
                seqQueue   = new LinkedBlockingQueue<MPacket>();
                NSQueue    = new LinkedBlockingQueue<MPacket>();
                ackQueue   = new LinkedBlockingQueue<MPacket>();
                
                
                
                
                tokenQueue   = new LinkedBlockingQueue<MPacket>();
                tokenAckQueue   = new LinkedBlockingQueue<MPacket>();
                
                if(myId == 0){
                	//create the token packet
                	MPacket token = new MPacket(name, MPacket.TOKEN, MPacket.SEND_TOKEN);
                	try {
                		if(Debug.debug || Debug.terse) System.out.println("Creating the token for the first time" + name);
						tokenQueue.put(token);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
   
                //Initialize hash table of clients to client name 
                clientTable = new Hashtable<String, Client>(); 
                
                // Create the GUIClient and connect it to the KeyListener queue
                //RemoteClient remoteClient = null;
                for(Player player: ns_resp.players){  
                        if(player.name.equals(name)){
                        	if(Debug.debug)System.out.println("Adding guiClient: " + player);
                                guiClient = new GUIClient(name, eventQueue);
                                maze.addClientAt(guiClient, player.point, player.direction);
                                this.addKeyListener(guiClient);
                                clientTable.put(player.name, guiClient);
                        }else{
                        	if(Debug.debug)System.out.println("Adding remoteClient: " + player);
                                RemoteClient remoteClient = new RemoteClient(player.name);
                                maze.addClientAt(remoteClient, player.point, player.direction);
                                clientTable.put(player.name, remoteClient);
                        }
                }
                
                // Use braces to force constructors not to be called at the beginning of the
                // constructor.
                /*
                {
                        maze.addClient(new RobotClient("Norby"));
                        maze.addClient(new RobotClient("Robbie"));
                        maze.addClient(new RobotClient("Clango"));
                        maze.addClient(new RobotClient("Marvin"));
                }
                */

                
                // Create the panel that will display the maze.
                overheadPanel = new OverheadMazePanel(maze, guiClient);
                assert(overheadPanel != null);
                maze.addMazeListener(overheadPanel);
                
                // Don't allow editing the console from the GUI
                console.setEditable(false);
                console.setFocusable(false);
                console.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder()));
               
                // Allow the console to scroll by putting it in a scrollpane
                JScrollPane consoleScrollPane = new JScrollPane(console);
                assert(consoleScrollPane != null);
                consoleScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Console"));
                
                // Create the score table
                scoreTable = new JTable(scoreModel);
                assert(scoreTable != null);
                scoreTable.setFocusable(false);
                scoreTable.setRowSelectionAllowed(false);

                // Allow the score table to scroll too.
                JScrollPane scoreScrollPane = new JScrollPane(scoreTable);
                assert(scoreScrollPane != null);
                scoreScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Scores"));
                
                // Create the layout manager
                GridBagLayout layout = new GridBagLayout();
                GridBagConstraints c = new GridBagConstraints();
                getContentPane().setLayout(layout);
                
                // Define the constraints on the components.
                c.fill = GridBagConstraints.BOTH;
                c.weightx = 1.0;
                c.weighty = 3.0;
                c.gridwidth = GridBagConstraints.REMAINDER;
                layout.setConstraints(overheadPanel, c);
                c.gridwidth = GridBagConstraints.RELATIVE;
                c.weightx = 2.0;
                c.weighty = 1.0;
                layout.setConstraints(consoleScrollPane, c);
                c.gridwidth = GridBagConstraints.REMAINDER;
                c.weightx = 1.0;
                layout.setConstraints(scoreScrollPane, c);
                                
                // Add the components
                getContentPane().add(overheadPanel);
                getContentPane().add(consoleScrollPane);
                getContentPane().add(scoreScrollPane);
                
                // Pack everything neatly.
                pack();

                // Let the magic begin.
                setVisible(true);
                overheadPanel.repaint();
                this.requestFocusInWindow();
        }
        
        
        
        private String getNextClient(HashMap<String,Integer> priority){
        	
        	//from the priority find who you are
        	int nextId = (myId+1)%priority.size();
            Iterator<Entry<String,Integer>> sets = priority.entrySet().iterator();
            for(;sets.hasNext();){
            	Entry<String,Integer> entry = sets.next();
            	String name = entry.getKey();
            	int id = entry.getValue();
            	//check if who you are
            	if(id == nextId){
            		return name;
            	}
            }
            return null;
        }
        

        /*
        *Starts the ClientSenderThread, which is 
         responsible for sending events
         and the ClientListenerThread which is responsible for 
         listening for events
        */
        private void startThreads(){
                //listen for new connections from other client
        		if(Debug.debug) System.out.println("Create Client Server");
                new Thread(new ClientServer(clientTable, seqQueue, clientQueue, myListenerSocket, socketHash, name, ackQueue,tokenQueue,nextClient,tokenAckQueue)).start();
                //Start a new sender thread server
                if(Debug.debug)System.out.println("Finished Creating Client Server\n");

 

                //Start a new listener thread for the server
                //if(Debug.debug)System.out.println("Create ClientListenerThread for server");
                //new Thread(new ClientListenerThread(mSocket, clientTable, seqQueue, clientQueue, socketHash, name, ackQueue,tokenQueue,nextClient,tokenAckQueue)).start(); 
                

                
                //create ns listener and sender
                //will need this when we need to tell NS that a client has died
                //new Thread (new NSSenderThread(mSocket, clientTable, NSQueue)).start();
                //new Thread (new NSListenerThread(mSocket, clientTable, NSQueue)).start();
                
                
                //create connection to other clients
                Iterator<Entry<String, ClientInfo>> sets = clientHash.entrySet().iterator();
                
                for(;sets.hasNext();){
                	Entry<String,ClientInfo> entry = sets.next();
                	String name = entry.getKey();
                	ClientInfo info = entry.getValue();
                	//now we have the addr and port to create socket
                	MSocket socket;
					try {
						socket = new MSocket(info.address,info.port);
						socketHash.put(name,socket);
						if(Debug.debug) System.out.println("Creating sockets");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
                }
                
                
                if(Debug.debug) System.out.println("Create ClientSenderThread for server");
                new Thread(new ClientSenderThread(eventQueue, seqQueue, socketHash, ackQueue,tokenQueue,nextClient,tokenAckQueue)).start();
                
                
                new Thread(new PacketConsumer(clientQueue, clientTable, name)).start(); 
        }

        /**
         * Entry point for the game.  
         * @param args Command-line arguments.
         */
        public static void main(String args[]) throws IOException,
                                        ClassNotFoundException{

             String host = args[0];
             int port = Integer.parseInt(args[1]);
             /* Create the GUI */
             Mazewar mazewar = new Mazewar(host, port);
             mazewar.startThreads();
        }
}
