import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.swing.Timer;
import java.util.EventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientSenderThread implements Runnable {

    private BlockingQueue<MPacket> eventQueue = null;
    private BlockingQueue<MPacket> seqQueue = null;
    private  HashMap <String, MSocket> socketHash = null;
    private InetAddress addr = null;
    private BlockingQueue <MPacket> ackQueue = null;
    private BlockingQueue <MPacket> tokenQueue = null;
    private BlockingQueue <MPacket> tokenAckQueue = null;
    private String nextClient = null;
    private int port = -1;
    private Hashtable <String, Boolean> ackHash = null;
    private int serverSeq = -1;

/*
    public ClientSenderThread(MSocket mSocket,
                              BlockingQueue eventQueue){
        this.mSocket = mSocket;
        this.eventQueue = eventQueue;
        
        try{
        	this.addr = InetAddress.getLocalHost();
        	this.port = mSocket.getLocalPort();
        }catch(UnknownHostException e){
        	e.printStackTrace();
        }
    }
    
    public ClientSenderThread(MSocket mSocket,
                              BlockingQueue eventQueue, BlockingQueue seqQueue){
        this.mSocket = mSocket;
        this.eventQueue = eventQueue;
        this.seqQueue = seqQueue;
        
        try{
        	this.addr = InetAddress.getLocalHost();
        	this.port = mSocket.getLocalPort();
        }catch(UnknownHostException e){
        	e.printStackTrace();
        }
    }
    */
    
    public ClientSenderThread(BlockingQueue _eventQueue, BlockingQueue _seqQueue, HashMap <String, MSocket> _socketHash, BlockingQueue <MPacket> _ackQueue){
        this.eventQueue = _eventQueue;
        this.seqQueue   = _seqQueue;
        this.socketHash = _socketHash;
        this.ackQueue   = _ackQueue;
        this.serverSeq = 0;
        
        if(socketHash == null){
        	if(Debug.debug)System.out.println("in ClientSenderThread constructor, socketHash = null");
        }
        else{
        	if(Debug.debug)System.out.println("in ClientListenerThread constructor, socketHash NOT null");
        }

        
    }
    
    
    public ClientSenderThread( BlockingQueue _eventQueue, BlockingQueue _seqQueue, HashMap <String, MSocket> _socketHash, BlockingQueue <MPacket> _ackQueue,BlockingQueue <MPacket> _tokenQueue,String _nextClient,BlockingQueue<MPacket> _tokenAckQueue){
        this.eventQueue = _eventQueue;
        this.seqQueue   = _seqQueue;
        this.socketHash = _socketHash;
        this.ackQueue   = _ackQueue;
        this.tokenQueue = _tokenQueue;
        this.nextClient = _nextClient;
        this.serverSeq = 0;
        this.tokenAckQueue = _tokenAckQueue;
        
        System.out.println("in ClientSenderThread constructor");
        
        if(socketHash == null){
        	  System.out.println("in ClientSenderThread constructor, socketHash = null");
        }
        else{
              System.out.println("in ClientListenerThread constructor, socketHash NOT null");
        }

    }
    
    public Boolean isTimeout(long timestamp,long duration){
    	
        long current = System.currentTimeMillis();
        return (current > timestamp + duration);
   
    }
    
    public int getSeqNum(){
    	
    	if(Debug.debug || Debug.terse) System.out.println("Trying to get a sequence number");
    	
    	int sequence = -1;
    	try {
			MPacket token = tokenQueue.take();
			sequence = token.sequenceNumber;
			token.sequenceNumber++;
			//send the token to the next guy
			MSocket socket = socketHash.get(nextClient);
			socket.writeObject(token);
			if(Debug.debug || Debug.terse) System.out.println("After Getting sequence number Sent token to " + nextClient);
			long server_timestamp = System.currentTimeMillis();
            int received_ack = 0;
            while(received_ack < 1){
            	//check if received an ack
               	if(tokenAckQueue.peek() != null){
            		MPacket ack = tokenAckQueue.take();
            		//System.out.println("got an ack in tokenackqueu");
            		if (ack.sequenceNumber == token.tokenSequenceNumber){
            			if(Debug.debug || Debug.terse)System.out.println("After getting sequence number Received acks " + token + " of " + socketHash.size() + ": " + ack);
            			received_ack++;
            			break;
               		}
            	}
            	
            	if(isTimeout(server_timestamp,200)){
            		if(Debug.debug || Debug.terse)System.out.println ("RETRANSMIT: sending token packet to " + nextClient + " " + token);
            		socket.writeObject(token);
                    server_timestamp = System.currentTimeMillis();
            	}
            }
					
			return sequence;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	return -1;
    };
    
    
    public void sendToken(){
    	try {
    		//if(Debug.debug || Debug.terse) System.out.println("Token queue size: " + tokenQueue.size());
    		Thread.sleep(200);
			MPacket token = tokenQueue.peek();
			if(token != null){
				token = tokenQueue.take();
			}else{
				return;
			}
			//send the token to the next guy
			MSocket socket = socketHash.get(nextClient);
			//if(socket == null) return;
			socket.writeObject(token);
			if(Debug.debug || Debug.terse) System.out.println("Sent token to " + nextClient + " Not using it " + token);
			long server_timestamp = System.currentTimeMillis();
            int received_ack = 0;
            while(received_ack < 1){
            	//check if received an ack
               	if(tokenAckQueue.peek() != null){
            		MPacket ack = tokenAckQueue.take();
            		if (ack.sequenceNumber == token.tokenSequenceNumber){
            			if(Debug.debug || Debug.terse)System.out.println("Received acks " + token + " of " + socketHash.size() + ": " + ack);
            			received_ack++;
            			break;
            		}
            	}
            	
            	if(isTimeout(server_timestamp,200)){
            		if(Debug.debug || Debug.terse)System.out.println ("RETRANSMIT: sending token packet to " + nextClient + " " + token);
            		socket.writeObject(token);
                    server_timestamp = System.currentTimeMillis();
            	}
            }
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    public void run() {

        MPacket packet = null;
        MPacket toServer = null;
        MPacket fromServer = null;
        Boolean server_request  = false;
        ackHash = new Hashtable();

        try{
            InetAddress myAddr = InetAddress.getLocalHost();
        }catch (UnknownHostException e){
            e.printStackTrace();
        }

       if(Debug.debug) System.out.println("Starting ClientSenderThread");
    
    	
        while(true){
            try{  
                
                ackHash = new Hashtable();
                

              //  if(Debug.debug || Debug.terse)System.out.println ("Trying to get from eventqueue: " + packet);
                //Take packet from queue
                packet = (MPacket)eventQueue.peek();
                if(packet != null){
                	packet = (MPacket)eventQueue.take();
                                   
                
                	if(Debug.debug || Debug.terse)System.out.println ("got packet from eventqueue: " + packet);
                	//request for a sequence number from server
                	/*
                	toServer = new MPacket(MPacket.SEQ_REQ, addr);
                	toServer.port = this.port;
                	toServer.sequenceNumber = this.serverSeq++;
                	if(Debug.debug || Debug.terse)System.out.println ("sending packet SEQ_REQ to server: " + toServer);
                	//FIXME
                	mSocket.writeObjectNoError(toServer);
                	long server_timestamp = System.currentTimeMillis();
                	while(seqQueue.isEmpty()){
                		//wait for timeout and retransmit
               	
                	if(isTimeout(server_timestamp,150)){
                		if(Debug.debug || Debug.terse)System.out.println ("RETRANSMIT: sending SEQ_REQ packet to server: " + toServer);
                        mSocket.writeObject(toServer);
                        server_timestamp = System.currentTimeMillis();
                	}
                	fromServer = (MPacket)seqQueue.take();
                	if(Debug.debug)System.out.println("got packet from server" + fromServer);
                	 */
                
                	//Set the sequence number, internet address and local port that is bounded to this socket
                	packet.sequenceNumber = getSeqNum(); 
                	packet.ipAddress = this.addr;
                	packet.port = this.port;
                                
                	//traverse socket list and send it to all other clients
                	Iterator<Entry<String, MSocket >> sets = socketHash.entrySet().iterator();
                	if(Debug.debug)System.out.println("Number of hash entries in socketHash = " + socketHash.size());
                
                	for(;sets.hasNext();){
                		Entry<String,MSocket> entry = sets.next();
                		String name = entry.getKey();
                		MSocket socket = entry.getValue();
                		ackHash.put(name, false);
                		if(Debug.debug || Debug.terse)System.out.println("sending to client" + name + ", Packet: " + packet);
                		if(Debug.debug || Debug.terse)System.out.println("Client: " + name + " , Socket : " + socket);
                		socket.writeObject(packet);
                	}
                
                	//done sending get a timestamp
                	long timestamp = System.currentTimeMillis();
                
                	//check if received from each client
                	int received = 0;
                	if(Debug.debug)System.out.println("Waiting for ACKs");
                	if (Debug.debug)System.out.println("socket hash size = " + socketHash.size());
                	while(received < socketHash.size()){
                		if(ackQueue.peek() != null){
                			MPacket ack = ackQueue.take();
                			if (ackHash.get(ack.name) != null && ackHash.get(ack.name) == false && ack.sequenceNumber == packet.sequenceNumber){
                				if(Debug.debug)System.out.println("Received acks " + received + " of " + socketHash.size() + ": " + ack);
                				ackHash.put(ack.name, true);
                				received++;
                			}
                		}
                	
                	//check if timeout
                	if(isTimeout(timestamp,150) && received < socketHash.size()){
                		//retransmit packet for those who did not send an ack
                		Iterator<Entry<String, Boolean >> ack_sets = ackHash.entrySet().iterator();
                        
                			for(;ack_sets.hasNext();){
                				Entry<String,Boolean> entry = ack_sets.next();
                				String name = entry.getKey();
                				Boolean ack_received = entry.getValue();
                				MSocket socket = socketHash.get(name);
                				if(!ack_received){
                					if(Debug.terse)System.out.println("RETRANSMIT: sending to to client" + name + ", Packet: " + packet);
                					if(Debug.debug || Debug.terse)System.out.println("Client: " + name + " , Socket : " + socket);
                					//FIXME!!!
                					socket.writeObject(packet);
                				}
                			}
                			timestamp = System.currentTimeMillis();
                		
                		}
                	}
                	if(Debug.debug || Debug.terse)System.out.println("Received all acks");
                }else{
                	sendToken();
                }
                
                	//mSocket.writeObject(packet);    
                 
            }catch(InterruptedException e){
            	e.printStackTrace();
            	Thread.currentThread().interrupt();    
            }
            
        }
         
    }
   
}
