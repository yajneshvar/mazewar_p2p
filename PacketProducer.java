import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class PacketProducer implements Runnable {

   private PriorityBlockingQueue<MPacket> queue = null;
   private BlockingQueue <MPacket> seqQueue = null;
   private BlockingQueue <MPacket> ackQueue = null;
   private BlockingQueue <MPacket> tokenAckQueue = null;
   private MSocket mSocket = null;
   private HashMap <String, MSocket> socketHash = null;
   private String name = null;
   private Thread myThread;
   private BlockingQueue <MPacket> tokenQueue = null;
   private String nextClient = null;
   private int recentTokenId = -1;
/*
   public PacketProducer(PriorityBlockingQueue<MPacket> packet_queue, MSocket mSocket){
      this.queue = packet_queue;
      this.mSocket = mSocket;
   
   }
   */
   
   public PacketProducer(PriorityBlockingQueue<MPacket> _queue, BlockingQueue<MPacket> _seqQueue, MSocket _mSocket,HashMap <String, MSocket> _socketHash, String _name, BlockingQueue <MPacket> _ackQueue, String name){
      this.queue = _queue;
      this.mSocket = _mSocket;
      this.seqQueue = _seqQueue; 
      this.socketHash = _socketHash;
      this.ackQueue = _ackQueue;
      this.name = _name;
      myThread = Thread.currentThread();
   }
   
   public PacketProducer(PriorityBlockingQueue<MPacket> _queue, BlockingQueue<MPacket> _seqQueue, MSocket _mSocket,
		   HashMap <String, MSocket> _socketHash, String _name, BlockingQueue <MPacket> _ackQueue,BlockingQueue <MPacket> _tokenQueue,String _nextClient,BlockingQueue <MPacket> _tokenAckQueue){
	      this.queue = _queue;
	      this.mSocket = _mSocket;
	      this.seqQueue = _seqQueue; 
	      this.socketHash = _socketHash;
	      this.ackQueue = _ackQueue;
	      this.name = _name;
	      this.tokenQueue = _tokenQueue;
	      this.nextClient = _nextClient;
	      this.tokenAckQueue = _tokenAckQueue;
	      myThread = Thread.currentThread();
	   }


public void run() {
        MPacket received = null;
        if(Debug.debug) System.out.println("Starting Packet Producer");
        while(true){
            try{
            	if(Debug.debug || Debug.terse)System.out.println("PProducer-" + myThread + "("+ name + ")" +" ::Trying to read");
                received = (MPacket) mSocket.readObjectNoError();
                
                //FIXME
                
                //if (name == null){ name = received.name;
                
                //}
                if(Debug.debug || Debug.terse)System.out.println("PProducer-" + myThread + "("+ name + ")" + " ::Received packet: " + received);
                //Send ack for all received packet to the sender
                MSocket sender_socket = null;
                String sender = received.name;
                if(Debug.debug)System.out.println("PProducer-" + myThread + "("+ name + ")" + " ::Name of sender is : " + sender);
                if(socketHash != null){
                	sender_socket = socketHash.get(sender);
                }else{
                	if(Debug.debug)System.out.println("SocketHash is NULL");
                }
                
                if (received.type != MPacket.ACK && received.type != MPacket.ACK_TOKEN){
                	if(Debug.debug || Debug.terse)System.out.println("PProducer-" + myThread + "("+ name + ")" + " ::ACK: this should not print if ack: " + received);
                	MPacket ack = (received.type == MPacket.TOKEN) ? new MPacket(this.name,MPacket.ACK_TOKEN, MPacket.ACK_ACTION) : new MPacket(this.name,MPacket.ACK, MPacket.ACK_ACTION);
                	
                	ack.sequenceNumber = (received.type == MPacket.TOKEN) ? received.tokenSequenceNumber : received.sequenceNumber;
                
                	if(sender_socket != null){
                		if(Debug.debug || Debug.terse)System.out.println("PProducer-" + myThread + "("+ name + ")" + " ::Sending ack: " + ack);
                		if(Debug.debug || Debug.terse)System.out.println("Name : " + sender + "Socket : " + sender_socket);
                		sender_socket.writeObject(ack);
                	}
                	
                	if (received.type == MPacket.SEQ_RESP){
                		if(Debug.debug)System.out.println("PProducer-" + myThread + "("+ name + ")" + " ::putting packet on seqQueue: " + received);
                		seqQueue.put(received);
                    }else if(received.type == MPacket.TOKEN){
                    	if(Debug.debug || Debug.terse)System.out.println("PProducer-" + myThread + "("+ name + ")" + " Received a token!");
                    	if(received.tokenSequenceNumber > recentTokenId){
                    		if(Debug.debug || Debug.terse)System.out.println("PProducer-" + myThread + "("+ name + ")" + " ::putting packet on tokenQueue: " + received);
                    		recentTokenId = received.tokenSequenceNumber;
                    		received.tokenSequenceNumber++;
                    		received.name = name;
                    		tokenQueue.put(received);
                    	}
                    }else{
                    	
                		queue.put(received);
                		//System.out.println("Received in clientQueue " + received);
                	}
                }else if (received.type == MPacket.ACK_TOKEN){
                	tokenAckQueue.put(received);
                	//System.out.println("Received in tokenQueue " + received);
                }else{
                	ackQueue.put(received);
                	//System.out.println("Received in ackQueue " + received);
                }
                
//                System.out.println("Received " + received);
            }catch(IOException e){
                e.printStackTrace();
            //FIXME
            //}catch(ClassNotFoundException e){
            //    e.printStackTrace();
            }catch(InterruptedException e){
                e.printStackTrace();
            } 

      }
   }
}
