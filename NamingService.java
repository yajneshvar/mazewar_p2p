import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;
import java.net.ServerSocket;
import java.net.Socket;

public class NamingService {
    
	//The maximum of clients that will join
	//Server waits until the max number of clients to join 
    private static final int MAX_CLIENTS = 4;
    private MServerSocket serverSocket = null;
    private int clientCount; //The number of clients before game starts
    private ArrayList<ClientInfo> clientList = null;
    private BlockingQueue eventQueue = null; //A list of events
    private ClientInfo serverInfo = null;
    
    /*
    * Constructor
    */
    public NamingService(int port) throws IOException{
        clientCount = 0; 
        serverSocket = new MServerSocket(port);
        if(Debug.debug) System.out.println("Listening on port: " + port);
        clientList = new ArrayList<ClientInfo> (MAX_CLIENTS);
        eventQueue = new LinkedBlockingQueue<MPacket>();
        //serverInfo = new ClientInfo(host,server_port);
    }
    
    /*
    *Starts the listener and sender threads 
    */
    public void startThreads() throws IOException{
        //Listen for new clients
        while(clientCount < MAX_CLIENTS){
            //Start a new listener thread for each new client connection
            MSocket socket = serverSocket.accept();
            
            new Thread(new NamingServiceListenerThread(socket, eventQueue)).start();

            //create a new client info and store it

            if(Debug.debug)System.out.println("Address of the client is" + socket.getInetAddress());
            
            ClientInfo player_info = new ClientInfo(socket,socket.getInetAddress());
            player_info.port = socket.getPort(); // gets the remote port that it is connected too
            
            clientList.add(clientCount, player_info);                            
            
            clientCount++;
        }
        
        //Start a new sender thread 
        new Thread(new NamingServiceSenderThread(clientList, eventQueue)).start();    
    }

        
    /*
    * Entry point for server
    */
    public static void main(String args[]) throws IOException {
        if(Debug.debug) System.out.println("Starting the server");
        int port = Integer.parseInt(args[0]);
        //String server = args[1];
        //int server_port = Integer.parseInt(args[2]);
        NamingService service = new NamingService(port);      
        service.startThreads();    

    }
}
