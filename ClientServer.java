import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class ClientServer implements Runnable{
   
    private MServerSocket mServerSocket;
    private Hashtable<String, Client> clientTable;
    private BlockingQueue seqQueue;
    private BlockingQueue <MPacket> ackQueue = null;
    private BlockingQueue <MPacket> tokenQueue = null;
    private BlockingQueue <MPacket> tokenAckQueue = null;
    private PriorityBlockingQueue<MPacket> clientQueue = null;
    private HashMap <String, MSocket> socketHash = null;
    private String name = null;
    private String nextClient = null;

    public ClientServer(Hashtable<String, Client> _clientTable, BlockingQueue _seqQueue, PriorityBlockingQueue<MPacket> _clientQueue, MServerSocket _myListenerSocket, HashMap <String, MSocket> _socketHash, String _name, BlockingQueue<MPacket> _ackQueue, BlockingQueue<MPacket> _tokenQueue, String _nextClient,BlockingQueue <MPacket> _tokenAckQueue){
       this.clientTable = _clientTable;
       this.seqQueue = _seqQueue;
       this.mServerSocket = _myListenerSocket;
       this.clientQueue = _clientQueue;
       this.name = _name;
       this.socketHash = _socketHash;
       this.ackQueue = _ackQueue;
       this.tokenQueue = _tokenQueue;
       this.nextClient = _nextClient;
       this.tokenAckQueue = _tokenAckQueue;
    }

    public void run(){
        //Listen for new clients
        int clientCount = 0;
        MSocket mSocket;
        //FIXME
        while(true){
            //Start a new listener thread for each new client connection
            try{
                mSocket = mServerSocket.accept();
                if(Debug.debug)System.out.println("Creating clientListenerThread in Client server");
                if(Debug.debug)System.out.println("Number of hash entries in socketHash = " + socketHash.size());
        		new Thread(new ClientListenerThread(mSocket, clientTable, seqQueue, clientQueue, socketHash, name, ackQueue,tokenQueue,nextClient,tokenAckQueue)).start();
        		if(Debug.debug)System.out.println("finished creating clientListenerThread in Client server");
                clientCount++;
                
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

  //  public int getPort(){
   //     return mServerSocket.getPort();
    //}
};
