import java.io.IOException;
import java.util.concurrent.BlockingQueue;
//import java.net.Socket;

public class NamingServiceListenerThread implements Runnable {

    private MSocket socket =  null;
    private BlockingQueue eventQueue = null;

    public NamingServiceListenerThread(MSocket socket, BlockingQueue eventQueue){
        this.socket = socket;
        this.eventQueue = eventQueue;
    }

    public void run() {
        MPacket received = null;
        if(Debug.debug) System.out.println("Starting a listener");
        while(true){
            try{
                received = (MPacket) socket.readObject();
                if(Debug.debug) System.out.println("Received: " + received);
                eventQueue.put(received);    
            }catch(InterruptedException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }
            
        }
    }
}
