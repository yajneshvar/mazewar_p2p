import java.io.IOException;

import java.net.InetAddress;
import java.io.Serializable;
//import java.net.Socket;

public class ClientInfo implements Serializable{
    public MSocket socket = null;
    public InetAddress address = null ;
    public int port;
    public String hostAddr = null;

    public ClientInfo(MSocket sock, InetAddress addr){
        this.socket  = sock;
        this.address = addr;
    }
    public ClientInfo(InetAddress addr, int port){
        this.address = addr;
        this.port    = port;
    }
    public ClientInfo(String host, int port){
    	this.hostAddr = host;
    	this.port = port;
    }
}


