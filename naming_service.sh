#!/bin/bash
JAVA_HOME=/usr
if [ "$#" -ne 1 ]; then
  #  echo "Usage: ./naming_server.sh  <port> <server> <server_port>"
    echo "Usage: ./naming_server.sh  <port>"
    exit 1
fi

#${JAVA_HOME}/bin/java NamingService  $1 $2 $3
${JAVA_HOME}/bin/java NamingService  $1 

