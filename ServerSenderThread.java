import java.io.InvalidObjectException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.BlockingQueue;
import java.util.HashMap;
import java.util.Random;

public class ServerSenderThread implements Runnable {

    //private ObjectOutputStream[] outputStreamList = null;
    private MSocket[] mSocketList = null;
    private BlockingQueue eventQueue = null;
    private int globalSequenceNumber; 
    private int yajNumber;
    private HashMap <String, MPacket> socketHash;
 
    public ServerSenderThread(MSocket[] mSocketList,
                              BlockingQueue eventQueue){
        this.mSocketList = mSocketList;
        this.eventQueue = eventQueue;
        this.globalSequenceNumber = 0;
        this.yajNumber = 0;
        this.socketHash = null;
    }
    
    public ServerSenderThread(MSocket[] mSocketList,
            BlockingQueue eventQueue, HashMap <String, MPacket> socketHash){
    	this.mSocketList = mSocketList;
    	this.eventQueue = eventQueue;
    	this.globalSequenceNumber = 0;
    	this.socketHash = socketHash;
    	this.yajNumber = 0;
    }
    
    
    
    public MSocket find(InetAddress addr, int port){
    	for(MSocket socket: mSocketList){
    		if(socket.getInetAddress().equals(addr) && socket.getPort() == port) return socket;
    	}
    	
    	return null;
    }

    /*
     *Handle the initial joining of players including 
      position initialization
     */
    public void handleHello(){
        
        //The number of players
        int playerCount = mSocketList.length;
        Random randomGen = null;
        Player[] players = new Player[playerCount];
        HashMap <String,MSocket> socketHash = new HashMap();
        if(Debug.debug) System.out.println("In handleHello");
        MPacket hello = null;
        try{        
            for(int i=0; i<playerCount; i++){
                hello = (MPacket)eventQueue.take();
                //Sanity check 
                if(hello.type != MPacket.HELLO){
                    throw new InvalidObjectException("Expecting HELLO Packet");
                }
                if(randomGen == null){
                   randomGen = new Random(hello.mazeSeed); 
                }
                //Get a random location for player
                Point point =
                    new Point(randomGen.nextInt(hello.mazeWidth),
                          randomGen.nextInt(hello.mazeHeight));
                
                //Start them all facing North
                Player player = new Player(hello.name, point, Player.North);
                players[i] = player;
            }
            
            hello.event = MPacket.HELLO_RESP;
            hello.players = players;
            
            
            //broadcast the initial packet
            for(MSocket cSocket : mSocketList){
            	hello.ipAddress = cSocket.getLocalAddress();
            	cSocket.writeObject(hello);
            	
            }
            /*
            //find the appropriate socket
            if(Debug.debug) System.out.println("Sending " + hello);
            MSocket socket = find(hello.ipAddress,hello.port);
            socket.writeObject(hello);
            */
            
            
        }catch(InterruptedException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();    
        }catch(IOException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
    
    public void run() {
        MPacket packet = null;
        
        handleHello();
        
        while(true){
            try{
                //Take packet from queue to broadcast
                //to all clients
                packet = (MPacket)eventQueue.take();
                //Tag packet with sequence number and increment sequence number
                if (packet.type == MPacket.SEQ_REQ){
                	if(Debug.debug)System.out.println("Server got SEQ_REQ : " + packet);
                    packet.type = MPacket.SEQ_RESP;
                    packet.sequenceNumber = this.yajNumber++;
                }
                else {
                    packet.sequenceNumber = this.globalSequenceNumber++;
                }
                if(Debug.debug) System.out.println("Sending " + packet);
                //Send it to all clients
                
                
                if(packet.type == MPacket.SEQ_RESP){
                    //find the particular socket to the client which requested the sequence number\
                	packet.name = "Server";
                    MSocket socket = find(packet.ipAddress,packet.port);
                    socket.writeObjectNoError(packet);

                }
                   

            }catch(InterruptedException e){
            	if(Debug.debug)System.out.println("Throwing Interrupt");
                Thread.currentThread().interrupt();    
            }
            
        }
    }
}
