import java.util.*;
import java.io.InvalidObjectException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
//import java.util.Random;

public class NamingServiceSenderThread implements Runnable {

    //private ObjectOutputStream[] outputStreamList = null;
    private ArrayList<ClientInfo> clientList = null;
    private BlockingQueue eventQueue = null;
    private int globalSequenceNumber;
    private HashMap<String,ClientInfo> clientHash = null;
    private HashMap<String,Integer> priority = null;
    private ClientInfo serverInfo = null;
    
    public NamingServiceSenderThread(ArrayList<ClientInfo> clients, BlockingQueue eventQueue){
        this.clientList = clients;
        this.eventQueue = eventQueue;
        this.globalSequenceNumber = 0;
    }
    
    public ClientInfo find(InetAddress addr, int port){
if(Debug.debug)System.out.println("looking for addr:" + addr + ", port = " + port);
    	for (ClientInfo client: clientList){
if(Debug.debug)System.out.println("--> addr:" + client.address + ", port:" + client.port);
    		if((client.address.equals(addr)) && (client.port == port)) return client;
    	}
    	return null;
    }

    /*
     *Handle the initial joining of players including 
      position initialization
     */
    public void handleHello(){
        
        //The number of players
        clientHash = new HashMap();
        priority = new HashMap();
        int playerCount = clientList.size();
        Random randomGen = null;
        Player[] players = new Player[playerCount];
        if(Debug.debug) System.out.println("In handleHello");
        MPacket hello = null;
        try{        
            for(int i=0; i<playerCount; i++){
                hello = (MPacket)eventQueue.take();
                //Sanity check 
                if(hello.type != MPacket.NS_HELLO){
                    throw new InvalidObjectException("Expecting  NS HELLO Packet");
                }

                //from the packet find the client
               
                ClientInfo my_info  = find(hello.ipAddress,hello.port);
                InetAddress my_inet = my_info.address;
                if(Debug.debug)System.out.println("Address of clients contacting me is " + my_inet + " and the port is " + hello.port);
                
                String my_key = hello.name; 
                ClientInfo packet_info = new ClientInfo(my_inet,hello.listener_port);
                //this hash contains the host name, ip address and listening port to connect too
                clientHash.put(my_key, packet_info);
                //this hash is for determining who is the next client to receive the token
                if(Debug.debug)System.out.println("Priority " + hello.name + "ID is:" + i);
                priority.put(hello.name,i);
                
                if(randomGen == null){
                    randomGen = new Random(hello.mazeSeed); 
                 }
                 //Get a random location for player
                 Point point =
                     new Point(randomGen.nextInt(hello.mazeWidth),
                           randomGen.nextInt(hello.mazeHeight));
                 
                 //Start them all facing North
                 Player player = new Player(hello.name, point, Player.North);
                 players[i] = player;
            
            }
            hello.name = "Naming Service";
            hello.event = MPacket.NS_RESP;
            hello.clientHash = clientHash;
            hello.priority = priority;
           // hello.sAddr = serverInfo.hostAddr;
           // hello.sPort = serverInfo.port;
            hello.players = players;
            
            if(Debug.debug) System.out.println("Sending " + hello);
            
            for(ClientInfo clients: clientList){
                clients.socket.writeObjectNoError(hello);   
            }
            
        }catch(InterruptedException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();    
        }catch(IOException e){
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
    
    public void run() {
        MPacket toBroadcast = null;
        
        handleHello();
        /*
        while(true){
            try{
                //check if its a client hello, service it
                //add new client to the list (add ipaddress)
                //make mpacke, add all teh client, and send it to the new client 
                
                //Take packet from queue to broadcast
                //to all clients
                toBroadcast = (MPacket)eventQueue.take();
                //Tag packet with sequence number and increment sequence number
                toBroadcast.sequenceNumber = this.globalSequenceNumber++;
                if(Debug.debug) System.out.println("Sending " + toBroadcast);
                //Send it to all clients
                for(MSocket mSocket: mSocketList){
                    mSocket.writeObject(toBroadcast);
                }
            }catch(InterruptedException e){
if(Debug.debug)System.out.println("Throwing Interrupt");
                Thread.currentThread().interrupt();    
            }
            
        }
        */
    }
}
