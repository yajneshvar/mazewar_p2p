import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.management.ManagementFactory;
import java.util.Hashtable;
import java.util.concurrent.PriorityBlockingQueue;

public class PacketConsumer implements Runnable {


   private PriorityBlockingQueue<MPacket> queue = null;
   private Hashtable<String, Client> clientTable = null;
   private int seq_num;
   private Thread myThread;
   private String name = null;



   public PacketConsumer(PriorityBlockingQueue<MPacket> _packet_queue,Hashtable<String,Client> _clientTable, String _name){
      this.queue = _packet_queue;
      this.clientTable = _clientTable;
      this.seq_num = -1;
      //this.name = _name;
   
   }

   public MPacket get_packet(){
	  //FIXME
	  myThread = Thread.currentThread();
      
	  MPacket peeked = null;

      //System.out.println("looking for SN: " + (seq_num + 1));
      if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread + "("+ name + ")" + " ::Expecting sequence number :" +  (seq_num + 1) );
      do{
         peeked = queue.peek();
         while(peeked != null && peeked.sequenceNumber < (seq_num + 1)){
        	 try {
				MPacket take = queue.take();
				peeked = queue.peek();
				//System.out.println("Discarding packet: " + take);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        	 
         }
      }while(peeked == null || ( seq_num != -1 &&  peeked.sequenceNumber != (seq_num + 1) )); 
      
      try{
    	 if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread + "("+ name + ")" + " ::Attempting to get a packet");
         peeked = queue.take();
         //System.out.println("Consuming packet : " + peeked);
         if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread + "("+ name + ")" + " ::received sequence number:" + peeked.sequenceNumber);
         seq_num = peeked.sequenceNumber;
      }catch(InterruptedException ex){
         ex.printStackTrace();
      }
      return peeked;
   }

   public void run() {
   
        MPacket received = null;
        MPacket peek_packet = null;
        Client client = null;
        if(Debug.debug) System.out.println("PCONSUMER-" + myThread + "("+ name + ")" + " ::Starting Packet Consumer");
        while(true){
  //          try{
                //received = queue.take();
                received = get_packet();
                if(Debug.debug ||  Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::get_packet received: " + received);
                
                client = clientTable.get(received.name);
                if(received.event == MPacket.UP){
                    client.forward();
                    if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::Yaj " + received.sequenceNumber);
                }else if(received.event == MPacket.DOWN){
                    client.backup();
                    if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::Yaj " + received.sequenceNumber);
                }else if(received.event == MPacket.LEFT){
                    client.turnLeft();
                    if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::Yaj " + received.sequenceNumber);
                }else if(received.event == MPacket.RIGHT){
                    client.turnRight();
                    if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::Yaj " + received.sequenceNumber);
                }else if(received.event == MPacket.FIRE){
                    client.fire();
                    if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::Yaj " + received.sequenceNumber);
                }else if(received.event == MPacket.PROJECTILE){
                    client.projectile();
                    if(Debug.debug || Debug.terse)System.out.println("PCONSUMER-" + myThread +" ::Yaj " + received.sequenceNumber);
                }else{
                	if(Debug.debug || Debug.terse)System.out.println("ERROR" + received);
                    throw new UnsupportedOperationException();
                }
           // }
            //catch(InterruptedException ex){
            //    ex.printStackTrace();
            //}

        }
   }
 
}
