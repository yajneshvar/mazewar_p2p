import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class ClientListenerThread implements Runnable {

    private MSocket mSocket  =  null;
    private Hashtable<String, Client> clientTable = null;
    private BlockingQueue <MPacket> seqQueue = null;
    private PriorityBlockingQueue<MPacket> clientQueue = null;
    private BlockingQueue <MPacket> NSQueue = null;
    private HashMap <String, MSocket> socketHash = null;
    private BlockingQueue <MPacket> ackQueue = null;
    private BlockingQueue <MPacket> tokenQueue = null;
    private BlockingQueue <MPacket> tokenAckQueue = null;
    private String name = null;
    private String nextClient = null;
/*
    public ClientListenerThread( MSocket mSocket,
                                Hashtable<String, Client> clientTable, PriorityBlockingQueue<MPacket> clientQueue){
        this.mSocket = mSocket;
        this.clientTable = clientTable;
        this.clientQueue = clientQueue;
        if(Debug.debug) System.out.println("Instatiating ClientListenerThread");
    }
*/
 
  
    public ClientListenerThread( MSocket _mSocket,                                                                                           
                                Hashtable<String, Client> _clientTable, BlockingQueue _seqQueue, PriorityBlockingQueue<MPacket> _clientQueue,HashMap <String, MSocket> _socketHash,String _name,  BlockingQueue <MPacket> _ackQueue){
        this.mSocket = _mSocket;
        this.clientTable = _clientTable;
        this.seqQueue = _seqQueue;
        this.clientQueue = _clientQueue;
        this.socketHash = _socketHash;
        this.name = _name;
        this.ackQueue = _ackQueue;
        if(Debug.debug) System.out.println("Instatiating ClientListenerThread");
    }
    
    
    public ClientListenerThread( MSocket _mSocket,Hashtable<String, Client> _clientTable, BlockingQueue _seqQueue, PriorityBlockingQueue<MPacket> _clientQueue,HashMap <String, MSocket> _socketHash,String _name,  BlockingQueue <MPacket> _ackQueue, BlockingQueue<MPacket> _tokenQueue, String _nextClient,BlockingQueue <MPacket> _tokenAckQueue){
    	this.mSocket = _mSocket;
    	this.clientTable = _clientTable;
    	this.seqQueue = _seqQueue;
    	this.clientQueue = _clientQueue;
    	this.socketHash = _socketHash;
    	this.name = _name;
    	this.ackQueue = _ackQueue;
    	this.tokenQueue = _tokenQueue;
    	this.nextClient = _nextClient;
    	this.tokenAckQueue = _tokenAckQueue;
    	if(Debug.debug) System.out.println("Instatiating ClientListenerThread");
    }

    public void run() {
        MPacket received = null;
        Client client = null;
        if(Debug.debug) System.out.println("Starting ClientListenerThread");

        new Thread(new PacketProducer(clientQueue, seqQueue, mSocket, socketHash, name, ackQueue,tokenQueue,nextClient,tokenAckQueue)).start(); 
        //new Thread(new PacketConsumer(clientQueue, clientTable, name)).start(); 

//        while(true){
//            try{
//                received = (MPacket) mSocket.readObject();
//                System.out.println("Received " + received);
//                client = clientTable.get(received.name);
//                if(received.event == MPacket.UP){
//                    client.forward();
//                }else if(received.event == MPacket.DOWN){
//                    client.backup();
//                }else if(received.event == MPacket.LEFT){
//                    client.turnLeft();
//                }else if(received.event == MPacket.RIGHT){
//                    client.turnRight();
//                }else if(received.event == MPacket.FIRE){
//                    client.fire();
//                }else{
//                    throw new UnsupportedOperationException();
//                }    
//            }catch(IOException e){
//                e.printStackTrace();
//            }catch(ClassNotFoundException e){
//                e.printStackTrace();
//            }            
//        }
    }
}
